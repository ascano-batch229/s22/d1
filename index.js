// console.log("hello world");

// Array methods
// JS has built in function and methods for arrays.

// Mutator Methods

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

// push()
// Will add element to the end part of the array
// SYNTAX: arrayName.push()

console.log("Current Array: ");
console.log(fruits);
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated Array from push method");
console.log(fruits);

// Adding Multiple elements to an array
fruits.push("Avocado", "Guava");
console.log("Mutated Array from push method");
console.log(fruits);

// pop()
// Will remove array element to the end part of the array

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method");
console.log(fruits);

//unshift()
// add element in the beginning of an array
fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method");
console.log(fruits);

// shift()
// removes an element in the beginning part of an array
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method");
console.log(fruits);

// splice()
// simultaneously removes an element from a specified index number and adds elements
// SYNTAX: arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

fruits.splice(1, 2, "Lime" , "Cherry");
console.log("Mutated array from splice method");
console.log(fruits);

// sort()
// Re-arranges the array elements in alpha numberic order

fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);

// reverse()
// reverses order of array elements

fruits.reverse();
console.log("Mutated array from reverse method");
console.log(fruits);

// Non-mutator methods
// Unlike the mutator methods, non-mutator methods cannot modify the array.

let countries = ["US" , "PH" , "CAN", "SG" , "TH" , "PH" , "FR" , "DE"];

// indexof()
// Will return the index number of the first matching element
// if no element was found, JS will return -1
// SYNTAX: arrayName.indexOf(searchValue, fromIndex);

let firstIndex = countries.indexOf("PH");
console.log("Result of Index Method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of Index Method: " + invalidCountry);

// lastIndexOf()
// Returns index number of the last matching element in an array
// SYNTAX: arrayName.lastIndexOf(searchValue, fromIndex);

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of LastIndexOf Method: " + lastIndex);

// Getting the index number starting from specified index
let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of LastIndexStart Method: " + lastIndexStart);

// slice()
// Portion/slices elements from an array and return a new array
// Syntax: arrayName.slice(startingIndex, endingIndex)

let slicedArrayA = countries.slice(2);
console.log("Result from slice method");
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2,4);
console.log("Result from slice method");
console.log(slicedArrayB);

// slicing off elements starting from the last elements of an array
let slicedArrayC = countries.slice(-3);
console.log("Result from slice method");
console.log(slicedArrayC);

// toString()
// Returns an array as a string seperated by commas
// SYNTAX: arrayname.toString()

let stringArray = countries.toString();
console.log("Result from toString method");
console.log(stringArray);

// concat()
// Combines two arrays and returns the combined result
// SYNTAX: arrayA.concat(arrayB)

let taskArray1 = ["drink html" , "eat js"];
let taskArray2 = ["inhale css",  "breath sass"];
let taskArray3 = ["get git", "be noded"];

let task = taskArray1.concat(taskArray2);
console.log("Result from concat method");
console.log(task);

// Combining multiple arrays
console.log("Result from concat method");
let allTask = taskArray1.concat(taskArray2,taskArray3);
console.log(allTask);

// Combining arrays with elements
console.log("Result from concat method");
let combinedTask = taskArray1.concat("small express", "throw react");
console.log(combinedTask);

// join()
// Returns an array as a string seperated by specified separator string
// SYNTAX: arrayName.join('separatorString')

let users = [ "John", "Jane", "Joe", "Robert"];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));


// Iteration Methods

// forEach
// Similar to a for loop that iterates on each array elements
// SYNTAX: arrayName.forEach(function(individualElement)){statement}

allTask.forEach(function(task){
	console.log(task)
})

// Using forEach with conditional statement

let filteredTask = [];

allTask.forEach(function(task){
	if(task.length > 10){
		filteredTask.push(task);
	}
})

console.log("Result of filtered task");
console.log(filteredTask);

// map()
// This is useful for performing task where mutating/changing the elements are required.
// SYNTAX: let/const resultArray = arrayName.map(function(indivElement))

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number * number;
});

console.log("Orginal Array:");
console.log(numbers); // original is unaffected by map()
console.log("Result From Map method");
console.log(numberMap); // A new array is returned and stored in a variable.

// map() vs forEach
let numberForEach = numbers.forEach(function(number){
	return number * number;
})

console.log(numberForEach); // undefined 

// forEach(), loops over all items in the array as map(), but forEach() does not return new array.

// every()
// check if all elements in an array meet the given condition
// SYNTAX: let/const resultArray = arrayName.every(function(indivElement){condition})

let allValid = numbers.every(function(number){
	return (number < 3);
})

console.log("Result from every method");
console.log(allValid);

// some()
// checks if at least one element in array meets the condition
// SYNTAX: let/const resultArray = arrayName.some(function(indivElement){condition})

let someValid = numbers.some(function(number){
	return(number < 2);
})

console.log("Result from some method");
console.log(someValid);

// filter()
// Returns new array that contains elements which meet the given condition.
// Return an empty array if no elements were found.
// SYNTAX: let/const resultArray = arrayName.(function(indivElement){condition})

let filterValid = numbers.filter(function(number){
	return ( number < 3);
})

console.log("Result from filter method");
console.log(filterValid);

// includes()
// checks if the argument passed can be found in the array
// SYNTAX: arrayName.includes(<argumentToFind>)

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1);//true

let productFound2 = products.includes("Headset");
console.log(productFound2);

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
})

console.log(filteredProducts);

// reduce()
// Evaluated elements from left to right and returns/reduces the array into a single value.
// SYNTAX: let/const resultArray = arrayName.reduce(function(accumulator, currentValue){operation})

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn("Current Iteration: " + ++iteration);
	console.log("accumulator: " + x);
	console.log("CurrentValue: " + y);

	// The operation to reduce the array into signle value
	return x + y;
})

console.log("Result of reduced method: " + reducedArray);

let list = ["Hello", "Again", "World"];

let reducedJoin = list.reduce(function(x, y){
	return x + " " + y;
})

console.log("Result of reduced method: " + reducedJoin);